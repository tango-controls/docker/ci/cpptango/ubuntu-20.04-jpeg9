FROM registry.gitlab.com/tango-controls/docker/ci/cpptango/ubuntu-20.04:v10

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive

USER root

RUN apt-get update -qq &&                      \
    apt-get remove -y                          \
      libjpeg-turbo8-dev libjpeg-dev &&        \
    apt-get install -y --no-install-recommends \
      libjpeg9-dev &&                          \
    rm -rf /var/lib/apt/lists/*

USER tango
